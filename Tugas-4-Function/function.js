//1
function teriak(a="Halo sabners!"){
    return a;
}

console.log(teriak());
console.log("----------------------------------------");

//2
function kalikan(num1,num2){
    return num2*num2;
}

var num1 = 12;
var num2 = 4;

var hasikKali = kalikan(num1,num2);
console.log(hasikKali);
console.log("---------------------------------------------");

//3
function introduce(name,age,address,hobby){
    return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+" !";
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 
console.log("-------------------------------------------------");