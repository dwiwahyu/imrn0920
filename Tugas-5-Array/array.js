//1
console.log("ARRAY KE SATU")
function range(startNum, finishNum) {
    var array=[];
    
    if(startNum<finishNum){
        
        for(startNum;startNum<=finishNum;startNum++){
            array.push(startNum);
            array.sort(function (value1, value2) { return value1 - value2 } ) ; 
        } 
    } else if(startNum>finishNum){
        
        for(startNum;startNum>=finishNum;startNum--){
            array.push(startNum);
            array.sort(function (value1, value2) { return value2 - value1 } ) ;
         
        } 
    }else{
        array.push(-1);
    }
    return array;
}

// Code di sini
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("--------------------------------------------------------------------");


//2
console.log("ARAAY KE DUA")
function rangeWithStep(startNum1, finishNum1, step) {
    var simpan=[];
    if(startNum1<=finishNum1){
        for(startNum1;startNum1<=finishNum1;startNum1+=step){
            simpan.push(startNum1);
            simpan.sort(function (value1, value2) { return value1 - value2 } ) ; 
        }
    }else if(startNum1>=finishNum1){
        for(startNum1;startNum1>=finishNum1;startNum1-=step){
            simpan.push(startNum1);
            simpan.sort(function (value1, value2) { return value2 - value1 } ) ;
        }
    }
    return simpan;
}
// Code di sini
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("---------------------------------------------------------------------------------");

//3
console.log("ARRAY KE TIGA");
function sum(a=0, b, step1=1) {
    var simpan1= [];
    
    if(a<=b){
        
        for(a;a<=b;a+=step1){
            simpan1.push(a);
        }
        var sim = simpan1.reduce(function(a,b) {return a + b }, 0);
    }else if(a>=b){
        for(a;a>=b;a-=step1){
            simpan1.push(a);
        }
        var sim = simpan1.reduce(function(a,b) {return a + b }, 0);
    }else{
        simpan1.push(a);
        var sim = simpan1.reduce(function(a,b) {return a + b }, 0);
       
        
    }
    return sim;
}



console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("-----------------------------------------------");

//4

//contoh input
console.log("ARRAY KE EMPAT")
console.log("-----------------------------------------");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

 function dataHandling(input){
    var indek = 0;
    for(indek;indek < input.length;indek++){
        console.log(`
                Nomor ID : ${input[indek][0]}
                Nama Lengkap : ${input[indek][1]}
                TTL : ${input[2]} ${input[indek][3]}
                Hobby : ${input[indek][4]}

                `); 
    
    }
 }

 dataHandling(input);
 console.log("============================================================");

 //5
 console.log("ARRAY KE LIMA");
 function balikKata(kata){
     var tampung = "";
     for(i=kata.length -1; i>=0; i--){
         tampung+=kata[i];
     }
     return tampung;
 }
 console.log(balikKata("Kasur Rusak"));
 console.log(balikKata("SanberCode"));
 console.log(balikKata("Haji Ijah"));
 console.log(balikKata("racecar"));
 console.log(balikKata("i am sanbers"));
 console.log("==========================================================================");

 //6

 console.log("ARRAY KE ENAM");
 console.log("===========================================================================");
 var inp= ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membacs"];

 function dataHandling2(inp){

    var ye=inp[3].split("/");
    var ze = inp[3].split("/");
    var tanggal = ye[1];

     inp.splice(1,2,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung");
     inp.splice(4,1,"Pria","SMA Internasional Metro");
     
    console.log(inp);
    switch(tanggal){
        case '05': {console.log('Mei'); break;}
        case '06': {console.log('Juni'); break;}
        default: {console.log('tidak terjadi apa-apa'); }
    }

    console.log( ye.sort(function(value1, value2) {return value2 - value1}));
    console.log( n = ze.join("-"));
    console.log( o = inp[1].slice(0,15));
 }
dataHandling2(inp);
console.log("======================================================");
 
 