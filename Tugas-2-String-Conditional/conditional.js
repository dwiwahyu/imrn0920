// if else

var nama = "dwik";
var peran = "Penyihir";

if(nama != "" && peran != ""){
    if(peran == 'Penyihir'){
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi warewolf")
    } else if(peran == 'Guard'){
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi warewolf")
    }else if(peran == 'Warewolf'){
        console.log("Halo "+peran+" "+nama+", kamu akan memangsa setiap malam")
    } else{
        console.log("Halo "+peran+" "+nama+", kamu gausa main aja")
    }
} else if(nama !="" && peran ==""){
    console.log("Halo "+nama+", pilih peranmu untuk memainkan game!")
} else{
    console.log("nama harus diisi!")
}

console.log("-----------------------------------------------------------------------------");

//switch

var tanggal = 21
var bulan = 9
var tahun = 2020

if(tanggal>0 && tanggal<32 && bulan>0 && bulan <13){

switch(bulan){
    case 1 :{console.log(tanggal+" januari "+tahun);break;}
    case 2 : {console.log(tanggal+" februari "+tahun);break;}
    case 3 : {console.log(tanggal+" maret "+tahun);break;}
    case 4 : {console.log(tanggal+" april "+tahun);break;}
    case 5 : {console.log(tanggal+" mei "+tahun);break;}
    case 6 : {console.log(tanggal+" juni "+tahun);break;}
    case 7 : {console.log(tanggal+" juli "+tahun);break;}
    case 8 : {console.log(tanggal+" agustus "+tahun);break;}
    case 9 : {console.log(tanggal+" september "+tahun);break;}
    case 10 : {console.log(tanggal+" oktober "+tahun);break;}
    case 11 : {console.log(tanggal+" november "+tahun);break;}
    case 12 : {console.log(tanggal+" desember "+tahun);break;}

}

}else if(tanggal<0 || tanggal >31 && bulan<0 || bulan>12){
    console.log("pastikan tanggal antara 1 sampai 31 dan bulan antara 1 sampai 12")
} else{
    console.log("salah perintah!")
}
