//1
var word = "Java Script";
var second = "is";
var third = "awesome";
var fourth = "and";
var fifth = "I";
var sixth = "love";;
var seventh = "it!"

console.log(word.concat(" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh))
console.log("--------------------------------------------------------------------")

//2

var sentence = "I am going to be React Native Developer"

var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var third = sentence.substring(5,10);
var fourth = sentence.substring(11,13);
var fifthth = sentence.substring(14,16);
var sixth = sentence.substring(17,22);
var seventh = sentence.substring(23,29);
var eightth = sentence.substring(30);

console.log('first word : '+exampleFirstWord);
console.log('second word : '+exampleSecondWord);
console.log('third word : '+third);
console.log('fourt word : '+fourth);
console.log('fifth word : '+fifthth);
console.log('six word : '+sixth);
console.log('seven word : '+seventh);
console.log('eight word : '+eightth);
console.log("---------------------------------------------------------------------");


//3
var sentences2 = "wow javascript is so cool!";

var first1 = sentences2.substring(0,3);
var second2 = sentences2.substring(4,14);
var third3 = sentences2.substring(15,17);
var fourth4 = sentences2.substring(18,20);
var fifthth5 = sentences2.substring(21);

console.log("first word :" + first1);
console.log("second word :" + second2);
console.log("third word :" + third3);
console.log("fourth word :" + fourth4);
console.log("fifth word :" + fifthth5);
console.log("----------------------------------------------------------------------------");

console.log("first word :" + first1 +", with length: " + first1.length);
console.log("second word :" + second2 +", with length: " + second2.length);
console.log("third word :" + third3 +", with length: " + third3.length);
console.log("fourth word :" + fourth4 +", with length: " + fourth4.length);
console.log("fifth word :" + fifthth5 +", with length: " + fifthth5.length);
console.log("-------------------------------------------------------------------------------");
