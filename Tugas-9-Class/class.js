class Animal {
    // Code class di sini
    constructor(name){
        this.name=name;
        this.legs=4;
        this.cold_blooded=false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(name){
        super(name);
        this.legsApe=2;
    }

    yell(){
        console.log (`Auoooooooo`);
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
    }

    jump(){
        console.log(`hop hop`)
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("==============================");

// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
//   }
  
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 

  console.log("====================================");

//   class Clock {
//     // Code di sini
//     constructor({template}){
//         this.template=template
//         this.timer;

//     }

//     render=()=>{
//         this.date = new Date();
//         this.hours =  this.date.getHours()
//         if(this.hours<10) this.hours='0'+this.hours;

//         this.mins=  this.date.getMinutes()
//         if(this.mins<10) this.mins='0'+this.mins;

//         this.secs= this.date.getSeconds()
//         if(this.secs<10) this.secs='0'+this.secs;

//         this.output=this.template
//         .replace('h', this.hours)
//         .replace('m', this.mins)
//         .replace('s', this.secs);

//         console.log(this.output);
//     }

//     stop(){
//         clearInterval(this.timer);
//     }

//     start(){
//         this.render;
//         this.timer=setInterval(this.render,1000)
//     }


// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start();  

console.log("================================");

class Clock {
    // Code di sini
    constructor({template}){
        this.template=template
        this.timer;

    }

    render=()=>{
        let date = new Date();
        let hours =  date.getHours()
        if(hours<10) hours='0'+hours;

        let mins=  date.getMinutes()
        if(mins<10) mins='0'+mins;

        let secs= date.getSeconds()
        if(secs<10) secs='0'+secs;

        let output=this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer=setInterval(this.render,1000)
    }


}

var clock = new Clock({template: 'h:m:s'});
clock.start();  