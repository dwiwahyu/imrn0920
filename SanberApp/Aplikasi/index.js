import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import LoginScreen from './Login';
import RegisterScreen from './RegisterScreen'
import AboutScreen from './AboutScreen';
import SkillScreen from './SkillScreen';
import TemanScreen from './Api';
import Al_Quran from './Corona';
// import SkillScreen from './SkillScreen';
// import { ProjectScreen, AddScreen } from './OtherScreen'

// const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const SkillStack = createStackNavigator();
const TemanStack = createStackNavigator();
const Al_QuranStack = createStackNavigator(); 

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name='Home' component={AboutScreen} />
  </HomeStack.Navigator>
);

const SkillStackScreen = () => (
    <SkillStack.Navigator>
      <SkillStack.Screen name='Skill' component={SkillScreen} />
    </SkillStack.Navigator>
    
  );

  const TemanStackScreen = () => (
    <TemanStack.Navigator>
      <TemanStack.Screen name='Friend' component={TemanScreen} />
    </TemanStack.Navigator>
  );

  const Al_QuranStackScreen = () => (
    <Al_QuranStack.Navigator>
      <Al_QuranStack.Screen name='Al_Quran' component={Al_Quran} />
    </Al_QuranStack.Navigator>
  );
// const Tabs = createBottomTabNavigator();
// const TabsScreen = () => (
//   <Tabs.Navigator>
//     <Tabs.Screen name="Skill" component={SkillScreen} options={{title:'Test1'}} />
//     <Tabs.Screen name="Project" component={ProjectScreen} />
//     <Tabs.Screen name="Add" component={AddScreen} />
//     <Tabs.Screen name="Adda" component={LoginScreen} />
//   </Tabs.Navigator>
// );

// const Drawer = createDrawerNavigator();
// const DrawerScreen = () => (
//   <Drawer.Navigator initialRouteName="Home">
//     <Drawer.Screen name="Home" component={TabsScreen} />
//     <Drawer.Screen name="About" component={AboutScreen} />
//     <Drawer.Screen name="Login" component={LoginScreen} />
//   </Drawer.Navigator>
// );

const Drawerab = createDrawerNavigator();
const DrawerabScreen=()=>(
    <Drawerab.Navigator initialRouteName="Home">
    <Drawerab.Screen name="Home" component={HomeStackScreen} />
    <Drawerab.Screen name="Skill" component={SkillStackScreen} />
    <Drawerab.Screen name="Friends" component={TemanStackScreen}/>
    <Drawerab.Screen name="Al-Quran" component={Al_QuranStackScreen}/>
    <Drawerab.Screen name="Logout" component={LoginScreen} />
   </Drawerab.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Login" component={LoginScreen} />
    <RootStack.Screen name="Register" component={RegisterScreen} />
    <RootStack.Screen name="About" component={AboutScreen} />
    <RootStack.Screen name="Skill" component={SkillScreen} />
    <RootStack.Screen name="Al-Quran" component={Al_Quran} />
    <RootStack.Screen name="Awal" component={DrawerabScreen} />
    
    {/* <RootStack.Screen name="App" component={DrawerScreen} />
    <RootStack.Screen name="About" component={ProjectScreen} /> */}
  </RootStack.Navigator>
);

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <RootStackScreen />
      </NavigationContainer>
    );
  }
}
