import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Axios from 'axios';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://al-quran-8d642.firebaseio.com/data.json`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <FlatList
        data={this.state.data}
        renderItem={({ item }) =>
        
          <View style={styles.viewList}>
           <View style={styles.arab}>
              <Text style={styles.textItemLo}> {item.asma}</Text>
              {/* <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} /> */}
            </View>
            <View style={styles.isi}>
              <Text style={styles.textItemLogin}> {item.nama}</Text>
              <Text style={styles.textItemLogin}> {item.arti}</Text>
              <Text style={styles.textItemUrl}>Jumlah Ayat: {item.ayat}</Text>

            </View>
            <View style={styles.arti}>
              <Text style={styles.textItemL}>Nomor Urut: {item.nomor}</Text>
            </View>
          </View>
        }
        keyExtractor={data => data.nomor.toString()}
        // skill => skill.id.toString()
      />
    );
  }
}

const styles = StyleSheet.create({
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center'
  },
  Image: {
    width: 88,
    height: 80,
    borderRadius: 40,
    marginLeft : 10
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    fontSize: 14
  },
  textItemUrl: {
    fontWeight: 'bold',
    
    fontSize: 12,
    marginTop: 10,
    color: 'blue'
  },
  arab : {
      
      width : 70
  },
  isi : {
      paddingHorizontal : 15,
      width : 200,
  },
  textItemLo : {
      fontSize : 16
  },
  textItemL : {
      fontSize : 14,
      textAlign : 'center',
      color : 'blue'
  },
  arti : {
      alignContent : 'center'
  }
})