import React from 'react';
import { View, Text, Image, ScrollView, TextInput, StyleSheet, Button, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


export default function About({navigation}) {
    return (
        <ScrollView>
            <View style={styles.container} >
                <Text style={styles.judul}>Tentang Saya</Text>
                <FontAwesome5 name={'user-circle'}
                    size={200} solid
                    color="#3EC6FF"
                    style={styles.icon}
                />
                <Text style={styles.nama}>وحي</Text>
                <Text style={styles.kerjaan}>Web and Mobile Developer</Text>
                <View style={styles.kotak} >
                    <Text style={styles.juduldalem}>Portofolio</Text>
                    <View style={styles.kotakdalem} >
                        <View>
                            <FontAwesome5 name="gitlab" size={40} color="#3EC6FF" style={styles.icon} />
                            <Text style={styles.textdalem}>وحي@</Text>
                        </View>
                        <View>
                            <FontAwesome5 name="github" size={40} color="#3EC6FF" style={styles.icon} />
                            <Text style={styles.textdalem}>وحي@</Text>
                        </View>
                    </View>
                    <View style={styles.skill}>
                        <TouchableOpacity onPress={() => navigation.navigate('Skill')}>
                            <Text style={styles.tforgot}>My Skill</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.kotak} >
                    <Text style={styles.juduldalem}>Hubungi Saya</Text>
                    <View style={styles.kotakdalemver} >
                        <View style={styles.kotakdalemverhub}>
                            <View >
                                <FontAwesome5 name="facebook" size={40} color="#3EC6FF" style={styles.icon} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 10}}>
                                <Text style={styles.textdalem}>وحي@</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalemverhub}>
                            <View >
                                <FontAwesome5 name="instagram" size={40} color="#3EC6FF" style={styles.icon} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 10}} >
                                <Text style={styles.textdalem}>وحي@</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalemverhub}>
                            <View>
                                <FontAwesome5 name="twitter" size={40} color="#3EC6FF" style={styles.icon} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 10}}>
                                <Text style={styles.textdalem}>وحي@</Text>
                            </View>
                        </View>
                    </View>
                </View>

            </View>

        </ScrollView>
    );
}


const styles = StyleSheet.create({
    container: {
        marginTop: 64,

    },
    judul: {
        fontSize: 36,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    },
    icon: {
        textAlign: "center"
    },
    nama: {
        fontSize: 24,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    },
    kerjaan: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#3EC6FF",
        textAlign: "center",
        marginBottom : 7
    },
    kotak: {
        borderRadius: 10,
        borderColor: 'blue',
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#EFEFEF',
        marginBottom : 9,
        marginLeft : 10,
        marginRight : 10,
        elevation : 5
    },
    kotakdalem: {
        borderTopWidth: 2,
        marginBottom : 10,
        borderTopColor: '#003366',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    kotakdalemver: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    kotakdalemverhub: {
        height: 50,
        flexDirection: 'row',
        justifyContent: "center",
        marginBottom:6,
        marginTop : 8

    },
    juduldalem: {
        fontSize: 18,
        color: "#003366",
    },
    textdalem: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },
    tforgot : {
        marginTop : 5,
        marginLeft : 260,
        fontSize : 16,
        color : '#3EC6FF',
        fontWeight : 'bold'
    },
    skill : {
        borderTopWidth : 2,
        borderTopColor : '#fff'
    }


});