import { getAppLoadingLifecycleEmitter } from 'expo/build/launch/AppLoading';
import react from 'react';
import React from 'react';
import {View,Text, StyleSheet, Image, TouchableOpacity, FlatList,Font, TextInput, Button} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class Login extends react.Component{
    render(){   
        return(
            <View style={styles.container}>

                <View style={styles.ya}>
                <Image source ={require('./image/logo.png')} style={{height:102, width:375}}/>
                    <Text style={styles.login}>
                        Register
                    </Text>
                </View>
                    

                <View style={styles.input}>
                    <View style={styles.inputName}>
                    <Text style={styles.huruf}>
                        UserName
                    </Text>
                    <TextInput style={styles.kotak}/>
                    </View>

                    <View style={styles.inputName,{marginTop:16}}>
                    <Text style={styles.huruf}>
                        Email
                    </Text>
                    <TextInput style={styles.kotak} textContentType={"emailAddress"}/>
                    </View>

                    <View style={styles.inputName,{marginTop:16}}>
                    <Text style={styles.huruf}>
                        Password
                    </Text>
                    <TextInput style={styles.kotak} secureTextEntry={true} />
                    </View>

                    <View style={styles.inputName,{marginTop:16}}>
                    <Text style={styles.huruf}>
                        Ulangi Password
                    </Text>
                    <TextInput style={styles.kotak} secureTextEntry={true} />
                    </View>
                </View>

                <View style={styles.dwi}>
                <TouchableOpacity style={styles.btsub} >
                <Text style={styles.textbt}> Masuk </Text>
                </TouchableOpacity>
                <Text style={styles.atau}>atau</Text>
                <TouchableOpacity style={styles.btreg} >
                <Text style={styles.textbt}> Daftar ? </Text>
                </TouchableOpacity>
                   
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({

    container:{
        flex : 1,
        marginTop : 63,
        
    },
    ya : {
        
        alignItems : 'center'
    },

    login : {
        fontSize:24, 
        fontFamily:'Roboto', 
        color : '#003366',
        marginTop : 40,
        alignItems : 'center'
    },

    input : {
        alignItems : 'center',
        marginTop : 30
    },

    inputName : {
        
        alignItems : 'flex-start',
    },

    huruf : {
        fontFamily:'Roboto',
         fontSize : 16, 
         color : '#003366', 
         height : 25
    },

    kotak : {
         height: 40,
         width: 294, 
         borderColor: '#003366', 
         borderWidth: 1, 
         backgroundColor:'#FFFFFF'
    },

    dwi : {
        marginTop : 32,
        alignItems : 'center'
    },

    btsub : {
        padding : 8,
        backgroundColor : '#3EC6FF',
        borderRadius : 16,
        width : 140,
        alignItems : 'center',
        marginBottom : 8

    },

    btreg: {
        alignItems: "center",
        backgroundColor: "#003366",
        textDecorationColor: '#000',
        padding: 8,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 8,
        width: 140
    },

    atau : {
        fontSize : 18,
        color : '#3EC6FF'

    },

    textbt : {
        alignItems : 'center',
        color : 'white',
        fontSize : 14,
        fontWeight: "bold"
    }


    
})