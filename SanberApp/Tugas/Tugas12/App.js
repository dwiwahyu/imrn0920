import react from 'react';
import React from 'react';
import {View,Text, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Video_item from './Video_item';
import data from './data.json';

export default class App extends react.Component{
    render(){   
        return(
            // <View style={styles.container}>
            //     <Text>
            //         ini tugas component
            //     </Text>
            // </View>

            <View style={styles.container}>
                <View style = {styles.navBar}>
                    <Image source ={require('./images/logo.png')} style={{height:22, width:98}}/>
                
                <View style={styles.rightNav}>
                    <TouchableOpacity>
                    <Icon style={styles.navItems} name='search' size={27}/>
                    </TouchableOpacity>
                    
                    <TouchableOpacity>
                    <Icon style={styles.navItems} name='account-circle' size={27}/>
                    </TouchableOpacity>    
                </View>
                
                </View>

                <View style = {styles.body}>
                    
                    <FlatList 
                    data={data.items}
                    renderItem={(video)=><Video_item video={video.item}/>}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>}
                    />
                </View>
                
                <View style = {styles.tabBar}>
                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = 'home' size={27} />
                        <Text style = {styles.tabTittle}>Home</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = 'whatshot' size={27} />
                        <Text style = {styles.tabTittle}>Trending</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = 'subscriptions' size={27} />
                        <Text style = {styles.tabTittle}>Subcribe</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = 'folder' size={27} />
                        <Text style = {styles.tabTittle}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    // container:{
    //     flex : 1,
    //     justifyContent : 'center',
    //     alignItems : 'center',
    // }

    container:{
        flex : 1,
        
    },
    navBar : {
        height : 100,
        backgroundColor : 'white',
        elevation : 5,
        paddingHorizontal : 25,
        paddingTop : 50,
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'space-between'
    },

    rightNav : {
        flexDirection : 'row'
    },

    navItems : {
        marginLeft : 21
    },

    body : {
        flex : 1
    },

    tabBar : {
        backgroundColor : 'white',
        height : 60,
        borderTopWidth : 0.5,
        borderColor : '#E5E5E5',
        flexDirection : 'row',
        justifyContent : 'space-around'
    },

    tabItems : {
        alignItems : 'center',
        justifyContent : 'center'
    },

    tabTittle : {
        fontSize : 12,
        color : '#3c3c3c',
        paddingTop : 2
    }
})