import React from 'react';
import {
    Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity, KeyboardAvoidingView
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


export default function App() {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
                <View style={styles.containerView}>

                   <View style={styles.tulisan}>
                    <Text style={styles.logintext}>Welcome Back</Text>
                    <Text style={styles.logintext2}>Sign in to continue </Text>
                    </View>
                </View>

                <View style={styles.container2}>
               
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Email</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext2}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true} />
                    </View>

                    <Text style={styles.tforgot}>Forgot Password ?</Text>
                    <TouchableOpacity style={styles.btsign} >
                            <Text style={styles.textsign}>  Sign in </Text>
                        </TouchableOpacity>
                    <Text style={styles.tor}>-OR-</Text>


                    <View style={styles.kotaklogin}>
                        <TouchableOpacity style={styles.btlogin} >
                        <FontAwesome5 name="facebook" size={20} color="white" style={styles.icon} />
                            <Text style={styles.textbt}>  Facebook </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btreg} >
                        <FontAwesome5 name="google" size={20} color="white" style={styles.icon} />
                            <Text style={styles.textbt}> Google </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>


    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerView: {
        backgroundColor: '#fff',
        marginTop: 153,
        alignItems: 'baseline',
        flex: 1
    },
    container2 : {
        marginHorizontal : 20,
        marginTop : 20,
        marginBottom : 10,
        alignItems : 'baseline',
        backgroundColor : '#FFFFFF',
        borderRadius : 11,
        elevation : 1
    },
    tulisan : {
        alignItems : 'baseline'
    },
    logintext: {
        fontSize: 30,
        color: '#0C0423',
        marginTop: 20,
        fontFamily : 'Roboto',
        marginLeft : 25,
        marginBottom : 2,
        alignItems : 'baseline',
        fontWeight : 'bold'
    },
    logintext2: {
        fontSize: 12,
        color: '#4D4D4D',
        textAlign: "center",
        fontFamily : 'Roboto',
        marginLeft : 25
    },
    
    formtext: {
        color: '#4D4D4D',
        fontFamily : 'Roboto',
        marginTop : 30,
        fontSize : 12
    },
    formtext2: {
        color: '#4D4D4D',
        fontFamily : 'Roboto',
        marginTop : 31
    },
    atautext: {
        fontSize: 20,
        color: '#3EC6FF',
        textAlign: "center"
    },
    forminput: {
        marginHorizontal: 30,
        alignContent: 'center',
        width: 294,
        borderBottomWidth : 1,
        borderBottomColor : '#E6EAEE'
    },
    input: {
        height: 20,
        width : 231,
        color: '#4C475A',
        marginTop : 12,
        marginBottom : 8
    },
    vbutton: {
        marginHorizontal: 90,
        borderRadius: 10,
        marginVertical: 10,
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        width: 140,
        marginTop : 30,
        marginLeft : 24,
        flexDirection : 'row',
        justifyContent : 'space-around'
    },
    btsign : {
        alignItems: "center",
        backgroundColor: "#F77866",
        padding: 15,
        borderRadius: 6,
        marginHorizontal: 24,
        marginBottom: 10,
        marginTop : 30,
        width: 308
     
    },
    textsign : {
        color : '#FFFFFF',
        fontSize : 14
    },
    btreg: {
        alignItems: "center",
        backgroundColor: "#003366",
        textDecorationColor: '#000',
        padding: 10,
        borderRadius: 16,
        width: 140,
        marginTop : 30,
        marginLeft : 20,
        flexDirection : 'row',
        justifyContent : 'space-around'
    },
    textbt: {
        color: 'white',
        fontSize: 15,
        fontWeight: "bold",
    },
    kotaklogin: {
        marginTop: 10,
        alignItems: 'center',
        flexDirection : 'row',
        marginBottom : 30
    },
    tforgot : {
        marginTop : 20,
        marginLeft : 231,
        fontSize : 12,
        color : '#0C0423'
    },
    tor : {
        marginLeft : 166,
        fontSize : 15,
        marginTop : 20,
        color : '#4C475A'
    }
});