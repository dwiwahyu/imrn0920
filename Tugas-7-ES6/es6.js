//1
console.log("SATU")
console.log("=============================================");
console.log("ES5 \n")

const golde = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golde()

  console.log("=============================================");
  console.log("ES6 \n")

  golden=()=>{
      console.log("this is golden!!")
  }
  golden()
console.log("==============================================");



  //2
  console.log("DUA \n")
   console.log("=============================================");
   console.log("ES5 \n")

  const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
   
  
   console.log("=============================================");
   console.log("ES6 \n")
  
   const newFunctio = (firstNam, lastNam)=>{
    let ab = `${firstNam} ${lastNam}`
    console.log(ab)
    }
   
  //Driver Code 
  newFunctio("William", "Imoh")
    
//     console.log(newFunction("William", "Imoh").firs)
  
console.log("=============================================");
// const newFunct = (tName, lName)=>{  
//       tName,
//       lName,
//       ful=()=>{
//           const first={tName}
//           const last={lName}
//         console.log(`${first} ${last}`)
//       } 
//   }

  newFunct("dwi", "wahyu").ful()

console.log("==============================================");
//   //3
console.log("TIGA")
console.log("=====================================================");
console.log("ES5 \n")
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

//   const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// Driver code
//console.log(firstName, lastName, destination, occupation)
console.log("=====================================================");
console.log("ES6 \n")
  const {firstName,lastName,destination,occupation,spell} = newObject;
  console.log(firstName, lastName, destination, occupation);
  console.log("=======================================================");

  //4
  console.log("EMPAT")
  console.log("===================================================");
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  const combined = west.concat(east)
  let combine = [...west,...east]
 // Driver Code
  console.log(combined)
  console.log(combine)
  console.log("===================================================");

  //5
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

let after= `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. ut enim  ad minim veniam`
 
// Driver Code
console.log(before) 
console.log(after)