//1
var angka1 = 2;
var pertama = 20;
var kedua = 2;

var looping1 = "I love coding";
var looping2 = "I will become a mobile developer";

console.log("LOOPING PERTAMA")

while(angka1<=pertama){
    console.log(angka1+" - "+looping1);
    angka1 += 2;

}

console.log("--------------------------------------------");

while(angka1>=4){
    angka1 -= 2;
    console.log(angka1+" - "+looping2);
}
console.log("--------------------------------------------");

//2
var awal = 1;
var akhir = 20;
var genap = "Berkualitas";
var ganjil = "Santai";
var kelipatan = "I Love Coding";

console.log("LOOPING KEDUA");

for(awal; awal <= akhir ; awal++){
    if(awal%2!=0 && awal%3==0){
        console.log(awal+" - "+kelipatan)
    } else if(awal%2!=0){
        console.log(awal+" - "+ganjil)
    } else if(awal%2==0){
        console.log(awal+" - "+genap);
    }
}
console.log("------------------------------------------------");

//3
var c = "########";
console.log("LOOPING KETIGA")
for(var a = 1; a<5; a++){
    console.log(c);
}
console.log("---------------------------------------------");

//4
var tangga = "#";
var d=1;
var e=7;
var row = 7;

console.log('LOOING KEEMPAT');

for (var i = 1; i <= row; i++) {
	var result = '';
	for (var j = 1; j <= i; j++) {
		
    result += '*';
	}
	console.log(result);
}

console.log("----------------------------------");

//5
console.log("LOOPING KELIMA");
var putih = " # # # #"
var hitam = "# # # # "

for(var k=1; k<9; k++){
    if(k%2==0){
        console.log(hitam);
    } else if(k%2!=0){
        console.log(putih);
    }
}

console.log("-----------------------------------------");